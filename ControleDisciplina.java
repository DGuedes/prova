import java.util.ArrayList;


public class ControleDisciplina {
    
    //atributos  
  	private ArrayList<Disciplina> listaDisciplina;
  
    //construtor        
  	public ControleDisciplina(){
  	listaDisciplina = new ArrayList<Disciplina>();
  	}
            
    //m�todos  
  	public String adicionarDisciplina(Disciplina umaDisciplina){
  	listaDisciplina.add(umaDisciplina);
  	String mensagem = "Disciplina registrada com �xito";
  	return mensagem;
  	}
  	
  	public void exibirDisciplina(){
		for(Disciplina umaDisciplina: listaDisciplina){
			System.out.println("Nome: " + umaDisciplina.getNomeDisciplina() + " Codigo: " + umaDisciplina.getCodigoDisciplina());
		}
  	}
        
    public String removerDisciplina(Disciplina umaDisciplina){
        listaDisciplina.remove(umaDisciplina);
        String mensagemRemoverDisciplina = "Disciplina removida com �xito!";
        return mensagemRemoverDisciplina;
    }
        
    public Disciplina pesquisarDisciplina(String umNomeDisciplina) {
        for (Disciplina umaDisciplina: listaDisciplina){
            if (umaDisciplina.getNomeDisciplina().equalsIgnoreCase(umNomeDisciplina)) return umaDisciplina;
        }
        return null;
    }
}