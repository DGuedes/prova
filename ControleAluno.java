import java.util.ArrayList;

public class ControleAluno{
	
    //atributos
	private ArrayList<Aluno> listaAluno;	
        
    //construtor
	public ControleAluno(){
	listaAluno = new ArrayList<Aluno>();
	}        
	
    //metodos	
	public String adicionarAluno(Aluno umAluno){
		String mensagem = "Aluno adicionado com sucesso!";
		listaAluno.add(umAluno);
		return mensagem;
	}
	
    public String removerAluno(Aluno umAluno){
        listaAluno.remove(umAluno);
        String removeMensagem = "Aluno removido com sucesso!";
        return removeMensagem;
    }
    
    public void exibirAluno(){
    	for (Aluno umAluno:listaAluno){
			System.out.println("Nome: " + umAluno.getNomeAluno() +  " Matricula: " + umAluno.getMatriculaAluno());
		}
    }
    public Aluno pesquisarAluno(String umNomeAluno) {
        for (Aluno umAluno: listaAluno) {
            if (umAluno.getNomeAluno().equalsIgnoreCase(umNomeAluno)) return umAluno;
        }
        return null;
    }  
}