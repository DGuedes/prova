import java.io.*;

public class CadastroAlunoDisciplina{
	
	public static void main(String[] args) throws IOException{
	
	InputStream entradaSistema = System.in;
	InputStreamReader leitor = new InputStreamReader(entradaSistema);
	BufferedReader leitorEntrada = new BufferedReader(leitor);
	String entradaTeclado;	
	
	// instanciando objetos
	
	ControleDisciplina umControleDisciplina = new ControleDisciplina();   
	ControleAluno umControleAluno = new ControleAluno();	
	Aluno umAluno = new Aluno();
	Disciplina umaDisciplina = new Disciplina();
			
	String menuOpcao;
    String opcao1="1";
    String opcao2="2";
    String opcao3="3";
    String opcao4="4";
    String opcao5="5";
    String opcao6="6";
    String opcao7="7";
	//menu come�o
    do{
    	System.out.println("Entre com uma op��o:");
    	System.out.println("1. Adicionar Disciplina");
    	System.out.println("2. Adicionar Aluno");
    	System.out.println("3. Listar alunos");
    	System.out.println("4. Listar disciplinas");
    	System.out.println("5. Remover uma disciplina");
    	System.out.println("6. Remover um aluno");
    	System.out.println("7. Sair do programa");
	
    	entradaTeclado = leitorEntrada.readLine();
    	menuOpcao = entradaTeclado;
        
    		while(!menuOpcao.equals(opcao1) && !menuOpcao.equals(opcao2) && !menuOpcao.equals(opcao3) && !menuOpcao.equals(opcao4) && !menuOpcao.equals(opcao5) && !menuOpcao.equals(opcao6) && !menuOpcao.equals(opcao7)){
    			System.out.println("Erro! Op��o invalida.");
    			System.out.println("Entre com uma nova op��o:");
    			entradaTeclado = leitorEntrada.readLine();
    			menuOpcao = entradaTeclado;            
    		}
        
    		if(menuOpcao.equals(opcao1)){            
            
    			System.out.println("Digite o nome do Disciplina:");
    			entradaTeclado = leitorEntrada.readLine();
    			String umNomeDisciplina = entradaTeclado;
            
    			System.out.println("C�digo da Disciplina:");
    			entradaTeclado = leitorEntrada.readLine();
    			String umCodigoDisciplina = entradaTeclado;
            
    			Disciplina umaNovaDisciplina = new Disciplina(umNomeDisciplina, umCodigoDisciplina);
            
    			String mensagem = umControleDisciplina.adicionarDisciplina(umaNovaDisciplina);		
    		}
        
    		if(menuOpcao.equals(opcao2)){
            
    			System.out.println("Digite o nome do aluno:");
    			entradaTeclado = leitorEntrada.readLine();
    			String umNomeAluno = entradaTeclado;
    			System.out.println("Codigo da disciplina do aluno matriculado:");
    			entradaTeclado = leitorEntrada.readLine();
    			String umCodigoDisciplina = entradaTeclado;
    			System.out.println("Matr�cula do aluno:");
    			entradaTeclado = leitorEntrada.readLine();
    			String umaMatricula = entradaTeclado;

    			Aluno umNovoAluno = new Aluno(umNomeAluno, umaMatricula);
    			System.out.println(umControleAluno.adicionarAluno(umNovoAluno));
            
    		}
        
    		if(menuOpcao.equals(opcao3)){
    			System.out.println("Voce desejou ver os alunos matriculados");
    			umControleAluno.exibirAluno();            
    		}
        
    		if(menuOpcao.equals(opcao4)){
    			System.out.println("Voce desejou ver a lista de disciplinas.");
    			umControleDisciplina.exibirDisciplina();        	
    		}
        
    		if(menuOpcao.equals(opcao5)){
    			System.out.println("Voce desejou remover uma disciplina.");
    			System.out.println("Nome da disciplina:");
    			entradaTeclado = leitorEntrada.readLine();
    			String umNomeDisciplina = entradaTeclado;
    			umaDisciplina = umControleDisciplina.pesquisarDisciplina(umNomeDisciplina);
    			umControleDisciplina.removerDisciplina(umaDisciplina);
    			System.out.println(umControleDisciplina.removerDisciplina(umaDisciplina));        	
    		}
        
    		if(menuOpcao.equals(opcao6)){
    			System.out.println("Voce desejou remover um aluno.");
    			System.out.println("Nome do aluno:");
    			entradaTeclado = leitorEntrada.readLine();
    			String umNovoNome = entradaTeclado;
    			umAluno = umControleAluno.pesquisarAluno(umNovoNome);
    			umControleAluno.removerAluno(umAluno);
    			System.out.println(umControleAluno.removerAluno(umAluno));
    		}
        
    		if(menuOpcao.equals(opcao7)){
    			System.out.println("Voc� desejou sair do programa.");
    			break;
    		}              
    	}while(menuOpcao.equals(opcao1) || !menuOpcao.equals(opcao2) || !menuOpcao.equals(opcao3) || !menuOpcao.equals(opcao4) || !menuOpcao.equals(opcao5) || !menuOpcao.equals(opcao6));
		
	}
}