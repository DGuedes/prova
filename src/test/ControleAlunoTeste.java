package test;


import static org.junit.Assert.*;
import modelo.Aluno;
import modelo.ControleAluno;

import org.junit.Before;
import org.junit.Test;

public class ControleAlunoTeste {
	
	private ControleAluno umControleAluno;
	private Aluno umAluno;
	
	@Before
	public void setUp() throws Exception {
		umControleAluno = new ControleAluno();
		umAluno = new Aluno();
	}
	
	@Test
	public void TesteAdicionarAluno(){
		umAluno.setNomeAluno("TesteAdicionar");
		umControleAluno.adicionarAluno(umAluno);
		assertEquals("Aluno adicionado com sucesso!", umControleAluno.adicionarAluno(umAluno));
	
	}
	@Test
	public void TesteRemoverAluno(){
		umAluno.setNomeAluno("TesteRemover");
		umControleAluno.adicionarAluno(umAluno);
		assertEquals("Aluno removido com sucesso!",  umControleAluno.removerAluno(umAluno));
	
	}
}
