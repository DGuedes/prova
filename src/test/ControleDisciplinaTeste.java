package test;

import static org.junit.Assert.*;

import modelo.ControleDisciplina;
import modelo.Disciplina;

import org.junit.Before;
import org.junit.Test;

public class ControleDisciplinaTeste {

	private ControleDisciplina umControleDisciplina;
	private Disciplina umaDisciplina;
	
	@Before
	public void setUp() throws Exception {
		
		umControleDisciplina = new ControleDisciplina();
		umaDisciplina = new Disciplina();
	}
		
	
	@Test
	public void TesteAdicionarDisciplina(){
		umaDisciplina.setNomeDisciplina("TesteAdicionarDisciplina");
		umControleDisciplina.adicionarDisciplina(umaDisciplina);
		assertEquals("Disciplina registrada com Exito", umControleDisciplina.adicionarDisciplina(umaDisciplina));
	
	}
	
	@Test
	public void TesteRemoverDisciplina(){
		umaDisciplina.setNomeDisciplina("TesteRemoverDisciplina");
		umControleDisciplina.adicionarDisciplina(umaDisciplina);
		assertEquals("Disciplina removida com Exito!",  umControleDisciplina.removerDisciplina(umaDisciplina));
	}

}
