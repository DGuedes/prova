package test;

import static org.junit.Assert.*;

import modelo.Aluno;


import org.junit.Test;
import org.junit.Before;
public class AlunoTest {
	
	private Aluno umAluno;
	@Before
	public void setUp() throws Exception {
		umAluno = new Aluno();
		
	}

	@Test
	public void TesteGetNomeAluno() {
		umAluno.setNomeAluno("ALUNOTESTE");
		assertEquals("ALUNOTESTE",umAluno.getNomeAluno());
	}
	@Test 
	public void TesteGetMatriculaAluno(){
		umAluno.setMatriculaAluno("TESTEMATRICULA");
		assertEquals("TESTEMATRICULA",umAluno.getMatriculaAluno());
		
	}
	@Test
	public void TesteGetDisciplina(){
		umAluno.setDisciplinaAluno("TESTEDISCIPLINA");
		assertEquals("TESTEDISCIPLINA",umAluno.getDisciplinaAluno());
	}
	

}
