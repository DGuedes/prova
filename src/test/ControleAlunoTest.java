package test;

import static org.junit.Assert.*;

import modelo.Aluno;
import modelo.Disciplina;

import org.junit.Test;
import org.junit.Before;
public class ControleAlunoTest {
	
	private Aluno umAluno;
	private Disciplina umaDisciplina;
	
	@Before
	public void setUp() throws Exception {
		umAluno = new Aluno();
		umaDisciplina = new Disciplina();
	}

	@Test
	public void TesteGetNomeAluno() {
		umAluno.setNomeAluno("ALUNOTESTE");
		assertEquals("ALUNOTESTE",umAluno.getNomeAluno());
	}
	@Test 
	public void TesteGetMatriculaAluno(){
		umAluno.setMatriculaAluno("TESTEMATRICULA");
		assertEquals("TESTEMATRICULA",umAluno.getMatriculaAluno());
		
	}
	@Test
	public void TesteGetDisciplina(){
		umAluno.setDisciplinaAluno("TESTEDISCIPLINA");
		assertEquals("TESTEDISCIPLINA",umAluno.getDisciplinaAluno());
	}
	

}
