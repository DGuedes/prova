package test;

import static org.junit.Assert.*;

import modelo.Disciplina;

import org.junit.Before;
import org.junit.Test;

public class DisciplinaTest {

	private Disciplina umaDisciplina;
	
	@Before
	public void setUp() throws Exception {
		umaDisciplina = new Disciplina();
		
	}
	@Test
	public void TesteSetNomeDisciplina() {
		umaDisciplina.setNomeDisciplina("TESTENOMEDISCIPLINA");
		assertEquals("TESTENOMEDISCIPLINA",umaDisciplina.getNomeDisciplina());
	}
	@Test
	public void TesteSetCodigoDisciplina(){
		umaDisciplina.setCodigo("TESTECODIGODISCIPLINA");
		assertEquals("TESTECODIGODISCIPLINA",umaDisciplina.getCodigoDisciplina());
	}
	
	

}
