package modelo;

public class Aluno{

	private String nomeAluno;
	private String matriculaAluno;
    private String disciplinaAluno;
    
    //construtor        
    public Aluno(){
	}

	public Aluno(String umNomeAluno){
		nomeAluno = umNomeAluno;
	}

	public Aluno(String umNomeAluno, String umaMatricula){
		nomeAluno = umNomeAluno;
        matriculaAluno = umaMatricula;		
	}
	
	//metodos        
	public void setNomeAluno(String umNomeAluno){
		nomeAluno = umNomeAluno;
	}
	public String getNomeAluno(){
		return nomeAluno;
	}
	public void setMatriculaAluno(String umaMatricula){
		matriculaAluno = umaMatricula;
	}
	public String getMatriculaAluno(){
		return matriculaAluno;
	}
    public void setDisciplinaAluno(String umCodigoDisciplina){
        disciplinaAluno = umCodigoDisciplina;
    }
    public String getDisciplinaAluno(){
        return disciplinaAluno;
    }
}
